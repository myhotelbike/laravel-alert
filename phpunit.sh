#!/usr/bin/env bash

docker run --rm\
  --entrypoint=""\
  --volume "$(pwd):/var/www/html"\
  registry.gitlab.com/myhotelbike/laravel-app/8.3/local:1.6.3\
  su-exec web vendor/bin/phpunit
