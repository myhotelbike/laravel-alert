<?php

namespace MyHotelBike\Alert\Facades;

use Illuminate\Contracts\Support\MessageBag;
use \Illuminate\Support\Facades\Facade;

/**
 * Alert facade
 *
 * @method static void success(string $message);
 * @method static void info(string $message);
 * @method static void warning(string $message);
 * @method static void error(string $message);
 * @method static void add(string $level, string $message);
 * @method static \MyHotelBike\Alert\Alert flash()
 * @method static string dump()
 *
 * @see \MyHotelBike\Alert\Alert
 * @see MessageBag
 *
 * @author Hieu Le <letrunghieu.cse09@gmail.com>
 */
class Alert extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'alert';
    }

}
